import { useState } from 'react';
import { useRouter } from 'next/router';
import { usePersonInformation } from '../../../components/hooks/usePersonInformation';
const EditPerson = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [gender, setGender] = useState('');
  const [birthday, setBirthday] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    const newData = {
      name,
      birthday,
      phone,
      gender,
    };
    save(newData);
  };
  return (
    <form>
      <input
        onChange={(e) => setName(e.target.value)}
        type="text"
        placeholder="Name"
        value={name}
      />
      <input
        onChange={(e) => setPhone(e.target.value)}
        type="text"
        placeholder="Phone Number"
        value={phone}
      />
      <input
        onChange={(e) => setGender(e.target.value)}
        type="text"
        placeholder="Gender"
        value={gender}
      />
      <input
        onChange={(e) => setBirthday(e.target.value)}
        type="text"
        placeholder="Birthday"
        value={birthday}
      />
      <input onClick={handleSubmit} type="button" value={'submit'} />
    </form>
  );
};

export default EditPerson;
